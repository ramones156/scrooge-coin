import java.util.ArrayList;
import java.util.List;

public class TxHandler {
    UTXOPool utxoPool;

    /* Creates a public ledger whose current UTXOPool (collection of unspent
     * transaction outputs) is utxoPool. This should make a defensive copy of
     * utxoPool by using the UTXOPool(UTXOPool uPool) constructor.
     */
    public TxHandler(UTXOPool utxoPool) {
        this.utxoPool = new UTXOPool(utxoPool);
    }

	/* Returns true if 
	 * (1) all outputs claimed by tx are in the current UTXO pool,
	 * (2) the signatures on each input of tx are valid, 
	 * (3) no UTXO is claimed multiple times by tx, 
	 * (4) all of tx’s output values are non-negative, and
	 * (5) the sum of tx’s input values is greater than or equal to the sum of its output values;
	   and false otherwise.
	 */

    public boolean isValidTx(Transaction tx) {
        List<UTXO> claimedUTXOPool = new ArrayList<>();
        double inputSum = 0;
        double outputSum = 0;

        List<Transaction.Input> inputs = tx.getInputs();
        for (var i = 0; i < inputs.size(); i++) {
            var input = tx.getInput(i);
            UTXO utxo = new UTXO(input.prevTxHash, input.outputIndex);

            // (1) all outputs claimed by tx are in the current UTXO pool,
            // just check if pool contains
            if (!utxoPool.contains(utxo)) {
                return false;
            }

            // (2) the signatures on each input of tx are valid 
            var correspondingOutput = utxoPool.getTxOutput(utxo);
            var pubKey = correspondingOutput.address;

            if (!pubKey.verifySignature(tx.getRawDataToSign(i),input.signature)) {
                return false;
            }

            // (3) no UTXO is claimed multiple times by tx
            // no UTXO is claimed multiple times by tx
            // Keep track of Used UTXO's in Tx
            if (claimedUTXOPool.contains(utxo)) {
                claimedUTXOPool.add(utxo);
                return false;
            }
            claimedUTXOPool.add(utxo);

            inputSum += correspondingOutput.value;
        }

        // (4) all of tx’s output values are non-negative
        var outputs = tx.getOutputs();
        for (var output : outputs) {
            // can't be negative
            if (output.value < 0.0) {
                return false;
            }

            outputSum += output.value;
        }

        // (5) the sum of tx’s input values is greater than or equal to the sum of its output values and false otherwise.
        return inputSum >= outputSum;
    }

    /* Handles each epoch by receiving an unordered array of proposed
     * transactions, checking each transaction for correctness,
     * returning a mutually valid array of accepted transactions,
     * and updating the current UTXO pool as appropriate.
     */
    public Transaction[] handleTxs(Transaction[] possibleTxs) {
        var validTxs = new ArrayList<>();
        for (Transaction t : possibleTxs) {
            if (isValidTx(t)) {
                validTxs.add(t);
                //remove UTXO
                for (var input : t.getInputs()) {
                    var outputIndex = input.outputIndex;
                    var prevTxHash = input.prevTxHash;
                    var utxo = new UTXO(prevTxHash, outputIndex);
                    utxoPool.removeUTXO(utxo);
                }
                // add UTXO
                byte[] txHash = t.getHash();
                for (var i = 0; i < t.numOutputs(); i++) {
                    var utxo = new UTXO(txHash, i);
                    utxoPool.addUTXO(utxo, t.getOutput(i));
                }
            }
        }
        var validTxArray = new Transaction[validTxs.size()];
        return validTxs.toArray(validTxArray);
    }
} 
